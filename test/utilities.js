const nock = require('nock');

const lib = require('../lib');
const helper = require('./helper');

describe('Utilities', function () {
	before(function () {
		nock.disableNetConnect();
	});
	after(function () {
		nock.cleanAll();
		nock.enableNetConnect();
	});
	describe('#getDatetimeAsp', function () {
		it('should return an ASP.NET formatted date', async function () {
			let span = helper.nock().post('/utilities/getdatetime/asp', '"2020-02-06T18:16:59"').reply(200, '"\/Date(1581031019000-0500)\/"', { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliUtilities = new lib.Utilities(helper.clientConfig);
			let item = await cliUtilities.getDatetimeAsp(new Date('02/06/2020 18:16:59'));
			item.response.should.equal('/Date(1581031019000-0500)/');

		});
		it('should fail for error status code', async function () {
			let span = helper.nock().post('/utilities/getdatetime/asp', '"2020-03-06T18:16:59"').reply(400);
			span.isDone().should.be.true;
			let cliUtilities = new lib.Utilities(helper.clientConfig);
			let item = await cliUtilities.getDatetimeAsp(new Date('03/06/2020 18:16:59'));
			item.response.should.endWith('Wrong response.');
		});
	});
});