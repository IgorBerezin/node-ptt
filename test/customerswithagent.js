const nock = require('nock');

const lib = require('../lib');
const helper = require('./helper');

describe('CustomersWithAgent', function () {
	before(function () {
		nock.disableNetConnect();
	});
	after(function () {
		nock.cleanAll();
		nock.enableNetConnect();
	});
	describe('#get', function () {
		it('should return a customer', async function () {
			let span = helper.nock().get('/customerswithagent/1').reply(200, helper.test.customer, { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let item = await cliCustomersWithAgent.get('1');
			//throw new Error(JSON.stringify(item));
			item.response.CustomersKey.should.equal(1);
			item.response.AccountNumber.should.equal('Test Account');

		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/customerswithagent/2').reply(400);
			span.isDone().should.be.true;
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let item = await cliCustomersWithAgent.get('2');
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomersWithAgent.get('xxx');
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
	describe('#list', function () {
		it('should return a list', async function () {
			var span = helper.nock().get('/customerswithagent?Take=1&Skip=1').reply(200, helper.test.customers, { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let item = await cliCustomersWithAgent.list({ pageSize: 1, pageIndex: 2 });
			item.response.length.should.equal(1);
			item.response[0].CustomersKey.should.equal(2);
			item.response[0].AccountNumber.should.equal('Test Account 2');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/customerswithagent?Take=1&Skip=2').reply(400);
			span.isDone().should.be.true;
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let item = await cliCustomersWithAgent.list({ pageSize: 1, pageIndex: 3 });
			item.response.should.endWith('Wrong response.');

		});
	});
	describe('#create', function () {
		it('should create a customer', async function () {
			let customer = {
				'BillToLocation': {
					'AddressLine1': '105 Oak Rim Court',
					'Attention': 'tst',
					'City': 'LOS GATOS',
					'CountryCode': 840,
					'County': 'SANTA CLARA',
					'Description': 'Main Location',
					'Name': 'test edit 10',
					'State': 'CA',
					'Type': 'Physical',
					'ZipCode': '95032'
				},
				'Customer': {
					'BillingType': 'Standard',
					'Status': 'New'
				}
			};

			let span = helper.nock().post('/customerswithagent', customer).reply(200, helper.test['create-customer'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let item = await cliCustomersWithAgent.create(customer);
			item.response.Customer.CustomersKey.should.equal(helper.test['create-customer'].Customer.CustomersKey);

		});
		it('should fail for error status code', async function () {
			let span = helper.nock().post('/customerswithagent', {}).reply(400);
			span.isDone().should.be.true;
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let item = await cliCustomersWithAgent.create({});
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomersWithAgent.create(null);
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong data.');
		});
	});
	describe('#update', function () {
		it('should update a customer', async function () {
			let customer = helper.test.customer;
			customer.AccountNumber = 'Test Account updated';

			let span = helper.nock().put('/customerswithagent/1', customer).reply(200, customer, { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let item = await cliCustomersWithAgent.update(customer);

			item.response.CustomersKey.should.equal(1);
			item.response.AccountNumber.should.equal('Test Account updated');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().put('/customerswithagent/2', { 'CustomersKey': 2}).reply(400);
			span.isDone().should.be.true;
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let item = await cliCustomersWithAgent.update({ 'CustomersKey': 2 });
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for missed argument', async function () {
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomersWithAgent.update(null);
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong data.');
		});
		it('should fail for wrong argument', async function () {
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomersWithAgent.update({});
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
	describe('#save', function () {
		it('should create a customer', async function () {
			let customer = {
				'BillToLocation': {
					'AddressLine1': '105 Oak Rim Court',
					'Attention': 'tst',
					'City': 'LOS GATOS',
					'CountryCode': 840,
					'County': 'SANTA CLARA',
					'Description': 'Main Location',
					'Name': 'test edit 10',
					'State': 'CA',
					'Type': 'Physical',
					'ZipCode': '95032'
				},
				'Customer': {
					'BillingType': 'Standard',
					'Status': 'New'
				}
			};

			let span = helper.nock().post('/customerswithagent', customer).reply(200, helper.test['create-customer'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let item = await cliCustomersWithAgent.save(customer);
			item.response.Customer.CustomersKey.should.equal(helper.test['create-customer'].Customer.CustomersKey);

		});
		it('should fail for error status code', async function () {
			let span = helper.nock().post('/customerswithagent', {}).reply(400);
			span.isDone().should.be.true;
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let item = await cliCustomersWithAgent.save({});
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for missed argument', async function () {
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomersWithAgent.save(null);
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong customer data.');
		});
		it('should update a customer', async function () {
			let customer = helper.test.customer;
			customer.AccountNumber = 'Test Account updated';

			let span = helper.nock().put('/customerswithagent/1', customer).reply(200, customer, { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let item = await cliCustomersWithAgent.save(customer);

			item.response.CustomersKey.should.equal(1);
			item.response.AccountNumber.should.equal('Test Account updated');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().put('/customerswithagent/2', { 'CustomersKey': 2 }).reply(400);
			span.isDone().should.be.true;
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let item = await cliCustomersWithAgent.save({ 'CustomersKey': 2 });
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomersWithAgent.save({ 'CustomersKey': 'xxx' });
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
		it('should fail for wrong BillToLocation argument', async function () {
			let cliCustomersWithAgent = new lib.CustomersWithAgent(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomersWithAgent.save({
					'BillToLocation': {
						'LocationsKey': 'xxx'
					},
					'Customer': {
						'CustomersKey': 'xxx'
					}
				});
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});

		//ToDo: { "Customer": {Customer Object}, "BillToLocation": {Location Object} } for update
	});
});
