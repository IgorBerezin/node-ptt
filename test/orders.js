const nock = require('nock');

const lib = require('../lib');
const helper = require('./helper');

describe('Orders', function () {
	before(function () {
		nock.disableNetConnect();
	});
	after(function () {
		nock.cleanAll();
		nock.enableNetConnect();
	});
    describe('#create', function () {
		it('should create an order', async function () {
			const data = {
				AssignedTo: "antonina.yushchenko",
	            CustomersKey: 8940,
	            Description: "Serial Number 653H9",
	            Expedite: 0,
	            OrderClassification: "MACD",
	            OrderSummary: "",
	            OrderType: "Add",
			};

            try {
                const span = helper.nock().post('/orders').reply(200, helper.test['hello-world'], { 'Content-Type': 'application/json' });
                span.isDone().should.be.true;
                const classWorker = new lib.Orders(helper.clientConfig);
                const item = await classWorker.create(data);
                item.response.hello.should.equal('world');
            } catch(err) {
                console.log(err);
                throw err;
            }
		});
        it('no data for create', async function () {
            try {
                const classWorker = new lib.Orders(helper.clientConfig);
                await classWorker.create(null);
            } catch(err) {
                if (err.message !== 'No data.')
                    throw err;
            }
		});
    });
});
