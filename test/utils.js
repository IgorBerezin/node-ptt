const utils = require('../lib/utils');

describe('utils', function () {
	describe('#joinURL', function () {
		it('should return URI', function () {
			utils.joinURL(['https://precisebillonline.com/', '/TBSDemo/RestService/TBSRestService']).should.equal('https://precisebillonline.com/TBSDemo/RestService/TBSRestService');
			utils.joinURL(['a', ' //b/ ', '?c=1']).should.equal('a/b?c=1');
		});
	});
	describe('#stringifyUnicode', function () {
		it('should return unicode value', function () {
			utils.stringifyUnicode('not unicode').should.equal('"not unicode"');
			utils.stringifyUnicode('игорь').should.equal('"\\u0438\\u0433\\u043e\\u0440\\u044c"');
		});
	});
	describe('#convertDateToLocalISOAspString', function () {
		it('should return date, converted to ASP or its own value', function () {
			utils.convertDateToLocalISOAspString(new Date('01/01/1975')).should.equal('1975-01-01T00:00:00');
			utils.convertDateToLocalISOAspString('000').should.equal('000');
		});
	});
	describe('#convertDateToLocalISOAspDate', function () {
		it('should return date, converted to ASP', function () {
			const now = new Date();
			const timeZone = now.toString().match(/([-\+][0-9]+)\s/)[1]; // for example +0200
			utils.convertDateToLocalISOAspDate(now).should.equal(`/Date(${now.valueOf()}${timeZone})/`);
		});
	});
	describe('#getSetDateFieldSettings', function () {
		it('should return JSON string of settings for Activate', function () {
			utils.getSetDateFieldSettings('Activate', '"\/Date(1581031019000-0500)\/"').should.equal('{"DesiredActiveDate":"\\"/Date(1581031019000-0500)/\\"","SetDateBehavior":"Apply-To-Items-Without-A-Current-Date"}');
		});
		it('should return JSON string of settings for Disconnect', function () {
			utils.getSetDateFieldSettings('Disconnect', '"\/Date(1581031019000-0500)\/"', 'reason').should.equal('{"DesiredDisconnectDate":"\\"/Date(1581031019000-0500)/\\"","DisconnectReason":"reason","SetDateBehavior":"Apply-To-Items-Without-A-Current-Date"}');
		});
		it('should return JSON string of settings for Reconnect', function () {
			utils.getSetDateFieldSettings('Reconnect', '"\/Date(1581031019000-0500)\/"').should.equal('{"CurrentDisconnectDate":"\\"/Date(1581031019000-0500)/\\""}');
		});
		it('should return JSON string of settings for other actions', function () {
			utils.getSetDateFieldSettings('Other', '"\/Date(1581031019000-0500)\/"').should.equal('{}');
		});
	});
});

