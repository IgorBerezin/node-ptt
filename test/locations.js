const nock = require('nock');

const lib = require('../lib');
const helper = require('./helper');

describe('Locations', function () {
	before(function () {
		nock.disableNetConnect();
	});
	after(function () {
		nock.cleanAll();
		nock.enableNetConnect();
	});
	describe('#get', function () {
		it('should return a location', async function () {
			let span = helper.nock().get('/locations/1').reply(200, helper.test['locations-get'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliLocations = new lib.Locations(helper.clientConfig);
			let item = await cliLocations.get('1');
			item.response.LocationsKey.should.equal(1);
			item.response.Name.should.equal('test location');

		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/locations/2').reply(400);
			span.isDone().should.be.true;
			let cliLocations = new lib.Locations(helper.clientConfig);
			let item = await cliLocations.get('2');
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliLocations = new lib.Locations(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliLocations.get('xxx');
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
	describe('#list', function () {
		it('should return a list', async function () {
			var span = helper.nock()
				.get('/locations?Take=1&Skip=1')
				.reply(200, helper.test['locations-list'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliLocations = new lib.Locations(helper.clientConfig);
			let item = await cliLocations.list({ pageSize: 1, pageIndex: 2 });
			item.response.length.should.equal(1);
			item.response[0].LocationsKey.should.equal(2);
			item.response[0].Name.should.equal('Test Location 2');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/locations?Take=1&Skip=2').reply(400);
			span.isDone().should.be.true;
			let cliLocations = new lib.Locations(helper.clientConfig);
			let item = await cliLocations.list({ pageSize: 1, pageIndex: 3 });
			item.response.should.endWith('Wrong response.');

		});
	});
	describe('#create', function () {
		it('should create a location', async function () {
			let location = {
				'AddressLine1': '105 Oak Rim Court',
				'City': 'LOS GATOS',
				'ContactEmail': 'ig.v.berezin@gmail.com',
				'ContactName': 'Igor Berezin',
				'CountryCode': 840,
				'County': 'SANTA CLARA',
				'Description': 'Main Location',
				'InCity': true,
				'Name': 'test create',
				'State': 'CA',
				'Type': 'Physical',
				'ZipCode': '95032'
			};

			let span = helper.nock()
				.post('/locations', location)
				.reply(200, helper.test['locations-create'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliLocations = new lib.Locations(helper.clientConfig);
			let item = await cliLocations.create(location);
			item.response.LocationsKey.should.equal(helper.test['locations-create'].LocationsKey);

		});
		it('should fail for error status code', async function () {
			let span = helper.nock().post('/locations', {}).reply(400);
			span.isDone().should.be.true;
			let cliLocations = new lib.Locations(helper.clientConfig);
			let item = await cliLocations.create({});
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliLocations = new lib.Locations(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliLocations.create(null);
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong data.');
		});
	});
	describe('#update', function () {
		it('should update a location', async function () {
			let location = Object.assign({}, helper.test['locations-get']);
			location.Name = 'Test Location updated';

			let span = helper.nock().put('/locations/1').reply(200, location, { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliLocations = new lib.Locations(helper.clientConfig);
			let item = await cliLocations.update(location);

			item.response.LocationsKey.should.equal(1);
			item.response.Name.should.equal('Test Location updated');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().put('/locations/2', { 'LocationsKey': 2}).reply(400);
			span.isDone().should.be.true;
			let cliLocations = new lib.Locations(helper.clientConfig);
			let item = await cliLocations.update({ 'LocationsKey': 2 });
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for missed argument', async function () {
			let cliLocations = new lib.Locations(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliLocations.update(null);
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong data.');
		});
		it('should fail for wrong argument', async function () {
			let cliLocations = new lib.Locations(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliLocations.update({});
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
	describe('#save', function () {
		it('should create a location', async function () {
			let location = {
				'AddressLine1': '105 Oak Rim Court',
				'City': 'LOS GATOS',
				'ContactEmail': 'ig.v.berezin@gmail.com',
				'ContactName': 'Igor Berezin',
				'CountryCode': 840,
				'County': 'SANTA CLARA',
				'Description': 'Main Location',
				'InCity': true,
				'Name': 'test create',
				'State': 'CA',
				'Type': 'Physical',
				'ZipCode': '95032'
			};

			let span = helper.nock()
				.post('/locations', location)
				.reply(200, helper.test['locations-create'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliLocations = new lib.Locations(helper.clientConfig);
			let item = await cliLocations.save(location);
			item.response.LocationsKey.should.equal(helper.test['locations-create'].LocationsKey);

		});
		it('should fail for error status code', async function () {
			let span = helper.nock().post('/locations', {}).reply(400);
			span.isDone().should.be.true;
			let cliLocations = new lib.Locations(helper.clientConfig);
			let item = await cliLocations.save({});
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for missed argument', async function () {
			let cliLocations = new lib.Locations(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliLocations.save(null);
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong location data.');
		});
		it('should update a location', async function () {
			let location = Object.assign({}, helper.test['locations-get']);
			location.Name = 'Test Location updated';

			let span = helper.nock().put('/locations/1', location).reply(200, location, { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliLocations = new lib.Locations(helper.clientConfig);
			let item = await cliLocations.save(location);

			item.response.LocationsKey.should.equal(1);
			item.response.Name.should.equal('Test Location updated');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().put('/locations/2', { 'LocationsKey': 2 }).reply(400);
			span.isDone().should.be.true;
			let cliLocations = new lib.Locations(helper.clientConfig);
			let item = await cliLocations.save({ 'LocationsKey': 2 });
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliLocations = new lib.Locations(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliLocations.save({ 'LocationsKey': 'xxx' });
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
	describe('#getDefaults', function () {
		it('should return a Location Defaults', async function () {
			let span = helper.nock()
				.get('/locations/defaults')
				.reply(200, helper.test['locations-defaults'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliLocations = new lib.Locations(helper.clientConfig);
			let item = await cliLocations.getDefaults();
			item.response.LocationsKey.should.equal(0);
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/locations/defaults').reply(400);
			span.isDone().should.be.true;
			let cliLocations = new lib.Locations(helper.clientConfig);
			let item = await cliLocations.getDefaults();
			item.response.should.endWith('Wrong response.');
		});
	});
});