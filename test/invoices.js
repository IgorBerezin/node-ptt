const nock = require('nock');

const lib = require('../lib');
const helper = require('./helper');

describe('Invoices', function () {
	before(function () {
		nock.disableNetConnect();
	});
	after(function () {
		nock.cleanAll();
		nock.enableNetConnect();
	});
	describe('#get', function () {
		it('should return an invoice', async function () {
			let span = helper.nock().get('/invoices/1').reply(200, helper.test['invoices-get'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliInvoices = new lib.Invoices(helper.clientConfig);
			let item = await cliInvoices.get('1');
			item.response.InvoicesKey.should.equal(1);
			item.response.Name.should.equal('test Invoice');

		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/invoices/2').reply(400);
			span.isDone().should.be.true;
			let cliInvoices = new lib.Invoices(helper.clientConfig);
			let item = await cliInvoices.get('2');
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliInvoices = new lib.Invoices(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliInvoices.get('xxx');
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
	describe('#list', function () {
		it('should return a list', async function () {
			var span = helper.nock()
				.get('/invoices?Take=1&Skip=1')
				.reply(200, helper.test['invoices-list'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliInvoices = new lib.Invoices(helper.clientConfig);
			let item = await cliInvoices.list({ pageSize: 1, pageIndex: 2 });
			item.response.length.should.equal(1);
			item.response[0].InvoicesKey.should.equal(2);
			item.response[0].Name.should.equal('Test Invoice 2');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/invoices?Take=1&Skip=2').reply(400);
			span.isDone().should.be.true;
			let cliInvoices = new lib.Invoices(helper.clientConfig);
			let item = await cliInvoices.list({ pageSize: 1, pageIndex: 3 });
			item.response.should.endWith('Wrong response.');

		});
	});
});