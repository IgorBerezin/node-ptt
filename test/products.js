const nock = require('nock');

const lib = require('../lib');
const helper = require('./helper');

describe('Products', function () {
	before(function () {
		nock.disableNetConnect();
	});
	after(function () {
		nock.cleanAll();
		nock.enableNetConnect();
	});
    describe('#create', function () {
		it('should create a product', async function () {
			const data = {
                ActiveCarrierNames: null,
                ActiveDate: "/Date(1674536400000-0500)/",
                CompanyKey: 1,
                CustomersKey: 8940,
                GrandparentProductKey: null,
                ParentProductID: null,
                ProductFormsKey: 21,
                ProductID: "653H9",        
			};

            try {
                const span = helper.nock().post('/products').reply(200, helper.test['hello-world'], { 'Content-Type': 'application/json' });
                span.isDone().should.be.true;
                const classWorker = new lib.Products(helper.clientConfig);
                const item = await classWorker.create(data);
                item.response.hello.should.equal('world');
            } catch(err) {
                console.log(err);
                throw err;
            }
		});
        it('no data for create', async function () {
            try {
                const classWorker = new lib.Products(helper.clientConfig);
                await classWorker.create(null);
            } catch(err) {
                if (err.message !== 'No data.')
                    throw err;
            }
		});
    });
});
