const nock = require('nock');

const lib = require('../lib');
const helper = require('./helper');

describe('Details', function () {
	before(function () {
		nock.disableNetConnect();
	});
	after(function () {
		nock.cleanAll();
		nock.enableNetConnect();
	});
    describe('#create', function () {
		it('should create a detail', async function () {
			const data = {
                ChargeClassification: "Telco",
                CustomersKey: 8940,
                DetailID: "91-19207-V",
                DetailsKey: 240203,
                IntervalCharge: 0.0,
                LkDetailsKey: 10,
                PlanKey: 1,
                PlanType: 10,
                Quantity: 19.0,
			};

            try {
                const span = helper.nock().post('/details').reply(200, helper.test['hello-world'], { 'Content-Type': 'application/json' });
                span.isDone().should.be.true;
                const classWorker = new lib.Details(helper.clientConfig);
                const item = await classWorker.create(data);
                item.response.hello.should.equal('world');
            } catch(err) {
                console.log(err);
                throw err;
            }
		});
        it('no data for create', async function () {
            try {
                const classWorker = new lib.Details(helper.clientConfig);
                await classWorker.create(null);
            } catch(err) {
                if (err.message !== 'No data.')
                    throw err;
            }
		});
    });
});