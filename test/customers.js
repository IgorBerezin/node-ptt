const nock = require('nock');

const lib = require('../lib');
const helper = require('./helper');

describe('Customers', function () {
	before(function () {
		nock.disableNetConnect();
	});
	after(function () {
		nock.cleanAll();
		nock.enableNetConnect();
	});
	describe('#get', function () {
		it('should return a customer', async function () {
			let span = helper.nock().get('/customers/1').reply(200, helper.test.customer, { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.get('1');
			item.response.CustomersKey.should.equal(1);
			item.response.AccountNumber.should.equal('Test Account');

		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/customers/2').reply(400);
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.get('2');
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomers.get('xxx');
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
	describe('#list', function () {
		it('should return a list', async function () {
			var span = helper.nock().get('/customers?Take=1&Skip=1').reply(200, helper.test.customers, { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.list({ pageSize: 1, pageIndex: 2 });
			item.response.length.should.equal(1);
			item.response[0].CustomersKey.should.equal(2);
			item.response[0].AccountNumber.should.equal('Test Account 2');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/customers?Take=1&Skip=2').reply(400);
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.list({ pageSize: 1, pageIndex: 3 });
			item.response.should.endWith('Wrong response.');

		});
	});
	describe('#create', function () {
		it('should create a customer', async function () {
			let customer = {
				'BillToLocation': {
					'AddressLine1': '105 Oak Rim Court',
					'Attention': 'tst',
					'City': 'LOS GATOS',
					'CountryCode': 840,
					'County': 'SANTA CLARA',
					'Description': 'Main Location',
					'Name': 'test edit 10',
					'State': 'CA',
					'Type': 'Physical',
					'ZipCode': '95032'
				},
				'Customer': {
					'BillingType': 'Standard',
					'Status': 'New'
				}
			};

			let span = helper.nock().post('/customers', customer).reply(200, helper.test['create-customer'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.create(customer);
			item.response.Customer.CustomersKey.should.equal(helper.test['create-customer'].Customer.CustomersKey);

		});
		it('should fail for error status code', async function () {
			let span = helper.nock().post('/customers', {}).reply(400);
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.create({});
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomers.create(null);
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong data.');
		});
	});
	describe('#update', function () {
		it('should update a customer', async function () {
			let customer = Object.assign({}, helper.test.customer);
			customer.AccountNumber = 'Test Account updated';

			let span = helper.nock().put('/customers/1', customer).reply(200, customer, { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.update(customer);

			item.response.CustomersKey.should.equal(1);
			item.response.AccountNumber.should.equal('Test Account updated');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().put('/customers/2', { 'CustomersKey': 2}).reply(400);
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.update({ 'CustomersKey': 2 });
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for missed argument', async function () {
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomers.update(null);
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong data.');
		});
		it('should fail for wrong argument', async function () {
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomers.update({});
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
	describe('#save', function () {
		it('should create a customer', async function () {
			let customer = {
				'BillToLocation': {
					'AddressLine1': '105 Oak Rim Court',
					'Attention': 'tst',
					'City': 'LOS GATOS',
					'CountryCode': 840,
					'County': 'SANTA CLARA',
					'Description': 'Main Location',
					'Name': 'test edit 10',
					'State': 'CA',
					'Type': 'Physical',
					'ZipCode': '95032'
				},
				'Customer': {
					'BillingType': 'Standard',
					'Status': 'New'
				}
			};

			let span = helper.nock().post('/customers', customer).reply(200, helper.test['create-customer'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.save(customer);
			item.response.Customer.CustomersKey.should.equal(helper.test['create-customer'].Customer.CustomersKey);

		});
		it('should fail for error status code', async function () {
			let span = helper.nock().post('/customers', {}).reply(400);
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.save({});
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for missed argument', async function () {
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomers.save(null);
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong customer data.');
		});
		it('should update a customer', async function () {
			let customer = Object.assign({}, helper.test.customer);
			customer.AccountNumber = 'Test Account updated';

			let span = helper.nock().put('/customers/1', customer).reply(200, customer, { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.save(customer);

			item.response.CustomersKey.should.equal(1);
			item.response.AccountNumber.should.equal('Test Account updated');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().put('/customers/2', { 'CustomersKey': 2 }).reply(400);
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.save({ 'CustomersKey': 2 });
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomers.save({ 'CustomersKey': 'xxx' });
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
		it('should fail for wrong BillToLocation argument', async function () {
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomers.save({
					'BillToLocation': {
						'LocationsKey': 'xxx'
					},
					'Customer': {
						'CustomersKey': 'xxx'
					}
				});
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});

		//ToDo: { "Customer": {Customer Object}, "BillToLocation": {Location Object} } for update
	});
	describe('#availablestatusactions', function () {
		it('should return a list of availablestatusactions', async function () {
			let span = helper.nock().get('/customers/1/availablestatusactions').reply(200, helper.test['customer-availablestatusactions'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.availablestatusactions('1');
			item.response.length.should.equal(2);
			item.response[0].Action.should.equal('Disconnect');
			item.response[1].Action.should.equal('Activate');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/customers/2/availablestatusactions').reply(400);
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.availablestatusactions('2');
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomers.availablestatusactions('xxx');
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
	describe('#performstatusaction', function () {
		it('should return a details of performstatusaction', async function () {
			let spanToPerform = helper.nock()
				.post('/customers/1/performstatusaction')
				.reply(200, helper.test['customer-performstatusaction-response'], { 'Content-Type': 'application/json' });
			spanToPerform.isDone().should.be.true;

			let spanToAvailable = helper.nock()
				.get('/customers/1/availablestatusactions')
				.reply(200, helper.test['customer-availablestatusactions'], { 'Content-Type': 'application/json' });
			spanToAvailable.isDone().should.be.true;

			let spanGetdatetime = helper.nock()
				.post('/utilities/getdatetime/asp', '"2020-02-06T18:16:59"')
				.reply(200, '"\/Date(1581031019000-0500)\/"', { 'Content-Type': 'application/json' });
			spanGetdatetime.isDone().should.be.true;


			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.performstatusaction('1', 'Activate', new Date('02/06/2020 18:16:59'));
			item.response.DetailsList.length.should.equal(1);
			item.response.DetailsList[0].Detail.should.equal('Workflow action was applied successfully on customer and 0 products.');
		});
		it('should fail for Workflow action is not available', async function () {
			let spanToPerform = helper.nock()
				.post('/customers/2/performstatusaction')
				.reply(200, helper.test['customer-performstatusaction-response'], { 'Content-Type': 'application/json' });
			let spanToAvailable = helper.nock()
				.get('/customers/2/availablestatusactions')
				.reply(200, helper.test['customer-availablestatusactions'], { 'Content-Type': 'application/json' });

			spanToPerform.isDone().should.be.true;
			spanToAvailable.isDone().should.be.true;

			let cliCustomers = new lib.Customers(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomers.performstatusaction('2', 'fake', new Date('02/06/2020 18:16:59'));
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Workflow action is not available.');
		});
		it('should fail for error status code', async function () {
			let spanToPerform = helper.nock()
				.post('/customers/3/performstatusaction')
				.reply(400);
			spanToPerform.isDone().should.be.true;

			let spanToAvailable = helper.nock()
				.get('/customers/3/availablestatusactions')
				.reply(200, helper.test['customer-availablestatusactions'], { 'Content-Type': 'application/json' });
			spanToAvailable.isDone().should.be.true;

			let spanGetdatetime = helper.nock()
				.post('/utilities/getdatetime/asp', '"2020-02-06T18:16:59"')
				.reply(200, '"\/Date(1581031019000-0500)\/"', { 'Content-Type': 'application/json' });
			spanGetdatetime.isDone().should.be.true;

			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.performstatusaction('3', 'Activate', new Date('02/06/2020 18:16:59'));
			item.response.should.endWith('Wrong response.');
		});
		it('should fail for wrong argument', async function () {
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomers.performstatusaction('xxx');
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
	describe('#disableInvoiceReview', function () {
		it('should return an InvoiceReview', async function () {
			let span = helper.nock()
				.post('/invoicereview')
				.reply(200, helper.test['invoicereview-create'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;

			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.disableInvoiceReview('1');
			item.response.InvoiceReviewKey.should.equal(1);
			item.response.CustomersKey.should.equal(1);
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().post('/invoicereview').reply(400);
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.disableInvoiceReview('2');
			item.response.should.endWith('Wrong response.');
		});
		it('should fail for wrong argument', async function () {
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomers.disableInvoiceReview('xxx');
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
	describe('#getLocations', function () {
		it('should return a list of locations', async function () {
			let span = helper.nock()
				.get('/customers/1/locations?Take=All')
				.reply(200, helper.test['locations-list'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.getLocations('1');
			item.response.length.should.equal(1);
			item.response[0].LocationsKey.should.equal(2);
			item.response[0].Name.should.equal('Test Location 2');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/customers/2/locations?Take=All').reply(400);
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.getLocations('2');
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomers.getLocations('xxx');
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
	describe('#getInvoices', function () {
		it('should return a list of invoices', async function () {
			let span = helper.nock()
				.get('/invoices?Take=All&Filter=(CustomersKey.Equal(1).OR.CorpAccountNumber.Equal(1))')
				.reply(200, helper.test['invoices-list'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.getInvoices('1');
			item.response.length.should.equal(1);
			item.response[0].InvoicesKey.should.equal(2);
			item.response[0].Name.should.equal('Test Invoice 2');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/invoices?Take=All&Filter=(CustomersKey.Equal(2).OR.CorpAccountNumber.Equal(2))').reply(400);
			span.isDone().should.be.true;
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let item = await cliCustomers.getInvoices('2');
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliCustomers = new lib.Customers(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliCustomers.getInvoices('xxx');
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
});
