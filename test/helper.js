const nock = require('nock');

const lib = require('../lib');
const test = require('./test');
const Client = lib.Client;

module.exports = {
	clientConfig: {
		'authorization': {
			'type': 'Basic',
			'username': 'FakeUserName',
			'password': 'FakePassword'
		},
		'url': {
			'host': 'https://precisebillonline.com/',
			'path': 'TBSDemo/RestService/TBSRestService'
		}
	},

	createClient: function () {
		return new Client(module.exports.clientConfig);
	},

	nock: function () {
		return nock('https://precisebillonline.com/TBSDemo/RestService/TBSRestService');
	},

	test: test
};