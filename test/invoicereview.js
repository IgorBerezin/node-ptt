const nock = require('nock');

const lib = require('../lib');
const helper = require('./helper');

describe('InvoiceReview', function () {
	before(function () {
		nock.disableNetConnect();
	});
	after(function () {
		nock.cleanAll();
		nock.enableNetConnect();
	});
	describe('#get', function () {
		it('should return a invoicereview', async function () {
			let span = helper.nock().get('/invoicereview/1').reply(200, helper.test['invoicereview-get'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliInvoiceReview = new lib.InvoiceReview(helper.clientConfig);
			let item = await cliInvoiceReview.get('1');
			item.response.InvoiceReviewKey.should.equal(1);
			item.response.CustomersKey.should.equal(1);

		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/invoicereview/2').reply(400);
			span.isDone().should.be.true;
			let cliInvoiceReview = new lib.InvoiceReview(helper.clientConfig);
			let item = await cliInvoiceReview.get('2');
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliInvoiceReview = new lib.InvoiceReview(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliInvoiceReview.get('xxx');
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
	describe('#list', function () {
		it('should return a list', async function () {
			var span = helper.nock().get('/invoicereview?Take=1&Skip=1').reply(200, helper.test['invoicereview-list'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliInvoiceReview = new lib.InvoiceReview(helper.clientConfig);
			let item = await cliInvoiceReview.list({ pageSize: 1, pageIndex: 2 });
			item.response.length.should.equal(1);
			item.response[0].InvoiceReviewKey.should.equal(1);
			item.response[0].Name.should.equal('Test');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/invoicereview?Take=1&Skip=2').reply(400);
			span.isDone().should.be.true;
			let cliInvoiceReview = new lib.InvoiceReview(helper.clientConfig);
			let item = await cliInvoiceReview.list({ pageSize: 1, pageIndex: 3 });
			item.response.should.endWith('Wrong response.');

		});
	});
	describe('#create', function () {
		it('should create an invoicereview', async function () {
			let invoicereview = {
				'CustomersKey': 1,
				'ReviewItems': null,
				'ReviewUntil': null,
				'T4UsersKey': null
			};

			let span = helper.nock().post('/invoicereview').reply(200, helper.test['invoicereview-create'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliInvoiceReview = new lib.InvoiceReview(helper.clientConfig);
			let item = await cliInvoiceReview.create(invoicereview);
			item.response.InvoiceReviewKey.should.equal(1);
			item.response.CustomersKey.should.equal(1);

		});
		it('should fail for error status code', async function () {
			let span = helper.nock().post('/invoicereview', {}).reply(400);
			span.isDone().should.be.true;
			let cliInvoiceReview = new lib.InvoiceReview(helper.clientConfig);
			let item = await cliInvoiceReview.create({});
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliInvoiceReview = new lib.InvoiceReview(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliInvoiceReview.create(null);
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong data.');
		});
	});
});