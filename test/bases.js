const nock = require('nock');

const lib = require('../lib');
const helper = require('./helper');

describe('Bases', function () {
	before(function () {
		nock.disableNetConnect();
	});
	after(function () {
		nock.cleanAll();
		nock.enableNetConnect();
	});
	describe('#get', function () {
		it('should return a Base', async function () {
			let span = helper.nock().get('/bases/1').reply(200, helper.test['bases-get'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliBases = new lib.Bases(helper.clientConfig);
			let item = await cliBases.get('1');
			item.response.LkBaseInfoKey.should.equal(1);
			item.response.Base.should.equal('Base');

		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/bases/2').reply(400);
			span.isDone().should.be.true;
			let cliBases = new lib.Bases(helper.clientConfig);
			let item = await cliBases.get('2');
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliBases = new lib.Bases(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliBases.get('xxx');
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
	describe('#list', function () {
		it('should return a list', async function () {
			var span = helper.nock().get('/bases?Take=1&Skip=1').reply(200, helper.test['bases-list'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliBases = new lib.Bases(helper.clientConfig);
			let item = await cliBases.list({ pageSize: 1, pageIndex: 2 });
			item.response.length.should.equal(1);
			item.response[0].LkBaseInfoKey.should.equal(1);
			item.response[0].Base.should.equal('Base');
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/bases?Take=1&Skip=2').reply(400);
			span.isDone().should.be.true;
			let cliBases = new lib.Bases(helper.clientConfig);
			let item = await cliBases.list({ pageSize: 1, pageIndex: 3 });
			item.response.should.endWith('Wrong response.');

		});
	});
	describe('#getCustomerDefaults', function () {
		it('should return a Customer Defaults', async function () {
			let span = helper.nock().get('/bases/1/customerdefaults').reply(200, helper.test['bases-customerdefaults'], { 'Content-Type': 'application/json' });
			span.isDone().should.be.true;
			let cliBases = new lib.Bases(helper.clientConfig);
			let item = await cliBases.getCustomerDefaults('1');
			item.response.CustomersKey.should.equal(0);
			item.response.BillOptions.CustomersKey.should.equal(0);
			item.response.CustomFields.CustomersKey.should.equal(0);
		});
		it('should fail for error status code', async function () {
			let span = helper.nock().get('/bases/2/customerdefaults').reply(400);
			span.isDone().should.be.true;
			let cliBases = new lib.Bases(helper.clientConfig);
			let item = await cliBases.getCustomerDefaults('2');
			item.response.should.endWith('Wrong response.');

		});
		it('should fail for wrong argument', async function () {
			let cliBases = new lib.Bases(helper.clientConfig);
			let error = new Error('Fake');
			try {
				await cliBases.getCustomerDefaults('xxx');
			} catch (e) {
				error = e;
			}
			error.message.should.equal('Wrong id.');
		});
	});
});