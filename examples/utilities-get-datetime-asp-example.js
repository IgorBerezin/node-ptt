const ptt = require('../');
const config = require('./config');


if (process.argv.length < 2) {
	console.log('usage: node utilities-get-datetime-asp-example [date]');
	process.exit(1);
}

const utilities = new ptt.Utilities(config);
const date = process.argv.length > 2 ? process.argv[2] : '';

const exec = async () => {
	try {
		let asp = await utilities.getDatetimeAsp(date);
		console.log('Result: ', asp);
	} catch (e) {
		console.log('Error: ' + e);
	}
};

exec().then();
