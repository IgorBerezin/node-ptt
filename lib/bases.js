const Client = require('./client');

/**
 * Main Bases package
 * Represents functionality to request Bases API.
 * @extends {Client}
 * @return {Bases}
 */
const Bases = module.exports = class Bases extends Client {

	/**
	 * Root api endpoint
	 */
	get Api() {
		return 'bases';
	}

	/**
	 * Retrieving a single record
	 * @param {Number} id LkBaseInfoKey
	 * @returns {Receipt}
	 */
	async get(id) {
		return super.get(this.Api, id);
	}

	/**
	 * Retrieving a collection of objects
	 * @param {Object} options Collection Parameters
	 * @param {Number||'All'} options.pageSize Maximum number of records in this collection {number or 'All'}
	 * @param {Number} options.pageIndex Will return those records that follow first X pages
	 * @param {Filter} options.filter Filters are used to limit the set of data returned in the collection
	 * @returns {Receipt}
	 */
	async list(options) {
		return await super.list(this.Api, options);
	}

	/**
	 * Retrieve Customer Defaults for a Base
	 * @param {Number} id LkBaseInfoKey
	 * @returns {Receipt}
	 */
	async getCustomerDefaults(id) {
		if (isNaN(id)) {
			throw new Error('Wrong id.');
		}
		return await this.execGetRequest(Client.Utils.joinURL([this.Api, id, '/customerdefaults']));
	}

};