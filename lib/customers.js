const Client = require('./client');
const Locations = require('./locations');
const Utilities = require('./utilities');
const InvoiceReview = require('./invoicereview');
const Invoices = require('./invoices');

/**
 * Main Customers package
 * Represents functionality to request Customers API.
 * @extends {Client}
 * @return {Customers}
 */
const Customers = module.exports = class Customers extends Client {

	/**
	 * Root api endpoint
	 */
	get Api() {
		return 'customers';
	}

	/**
	 * Retrieving a single record
	 * @param {Number} id CustomersKey
	 * @returns {Receipt}
	 */
	async get(id) {
		return super.get(this.Api, id);
	}

	/**
	 * Retrieving a collection of objects
	 * @param {Object} options Collection Parameters
	 * @param {Number||'All'} options.pageSize Maximum number of records in this collection {number or 'All'}
	 * @param {Number} options.pageIndex Will return those records that follow first X pages
	 * @param {Filter} options.filter Filters are used to limit the set of data returned in the collection
	 * @returns {Receipt}
	 */
	async list(options) {
		return await super.list(this.Api, options);
	}

	/**
	 * Creating a customer.
	 * @summary
	 * Create: requires an object consisting of the information
	 * for the Customer record as well as the information
	 * for the 'bill to' location:
	 * { "Customer": {Customer Object}, "BillToLocation": {Location Object} }
	 * @param {Object} customer Customer record
	 * @returns {Receipt}
	 */
	async create(customer) {
		if (!customer) {
			throw new Error('Wrong data.');
		}
		//no need to check the object structure because PTT will do it.
		return await this.execPostRequest(customer, this.Api);
	}

	/**
	 * Updating a customer.
	 * @summary
	 * Update: requires a Customer record
	 * {Customer Object}
	 * @param {Object} customer Customer record
	 * @returns {Receipt}
	 */
	async update(customer) {
		if (!customer) {
			throw new Error('Wrong data.');
		}

		if (isNaN(customer.CustomersKey)) {
			throw new Error('Wrong id.');
		}

		return await this.execPutRequest(customer, Client.Utils.joinURL([this.Api, customer.CustomersKey]));
	}

	/**
	 * Saving a customer.
	 * @summary
	 * Method may be used to Create or Update customer record.
	 *
	 * Create: requires an object consisting of the information
	 * for the Customer record as well as the information
	 * for the 'bill to' location:
	 * { "Customer": {Customer Object}, "BillToLocation": {Location Object} }
	 *
	 * Update: requires a Customer record only, but will works with a combined data also
	 * {Customer Object}
	 * { "Customer": {Customer Object}, "BillToLocation": {Location Object} }
	 * @param {Object} customer Customer record
	 * @returns {Receipt}
	 */
	async save(customer) {
		if (!customer) {
			throw new Error('Wrong customer data.');
		}

		if (customer.CustomersKey) {
			return await this.update(customer);
		} else {
			if (customer.Customer && customer.Customer.CustomersKey) {
				let locationResult = {};

				if (customer.BillToLocation) {
					let cliLocations = new Locations(this.Config);
					locationResult = await cliLocations.save(customer.BillToLocation);
				}

				if (locationResult.error) {
					return locationResult;
				}

				let customerResult = await this.update(customer.Customer);

				if (!customerResult.error) {
					//aggregated response of 2 requests
					customerResult.response = {
						Customer: customerResult.response
					};
					if (customer.BillToLocation) {
						customerResult.response.BillToLocation = locationResult.response;
					}
				}


				return customerResult;
			} else {
				return await this.create(customer);
			}
		}
	}

	/**
	 * GET the list of available “Actions” for the record you wish to change.
	 * @param {Number} id CustomersKey
	 * @returns {Receipt}
	 */
	async availablestatusactions(id) {
		if (isNaN(id)) {
			throw new Error('Wrong id.');
		}

		return await this.execGetRequest(Client.Utils.joinURL([this.Api, id, 'availablestatusactions']));
	}

	/**
	 * To perform the Status/Action Workflow.
	 * @summary
	 * Use availablestatusactions method to GET the list of available “Actions” for the record,
	 * that can be performed on the record based on it’s current Status and the configuration
	 * rules setup in your TBS system.
	 * @param {Number} id CustomersKey
	 * @param {String} action Action Code
	 * @param {Date} date Desired date
	 * @param {String} note Action note/reason
	 * @param {Boolean} applyToDescendants Indicates if children records should be affected.
	 * @returns {Receipt}
	 */
	async performstatusaction(id, action, date, note, applyToDescendants) {
		if (isNaN(id)) {
			throw new Error('Wrong id.');
		}

		let availablestatusactionsResponse = await this.availablestatusactions(id);
		if (availablestatusactionsResponse.error) {
			return availablestatusactionsResponse;
		}

		let availableAction = availablestatusactionsResponse.response.find(a => a.Action === action);

		if (!availableAction) {
			throw new Error('Workflow action is not available.');
		}

		let cliUtilities = new Utilities(this.Config);
		let getDatetimeAspResult = await cliUtilities.getDatetimeAsp(date);
		if (getDatetimeAspResult.error) {
			return getDatetimeAspResult;
		}

		let performstatusaction = {
			StandardSettings: {
				Action: availableAction.Action,
				ApplyToDescendants: applyToDescendants,
				SetDateFieldSettings: Client.Utils.getSetDateFieldSettings(availableAction.Action, getDatetimeAspResult.response, note)
			}
		};

		return await this.execPostRequest(performstatusaction, Client.Utils.joinURL([this.Api, id, 'performstatusaction']));
	}

	/**
	 * Do not include to Billing Review.
	 * @param {Number} id CustomersKey
	 * @returns {Receipt}
	 */
	async disableInvoiceReview(id) {
		if (isNaN(id)) {
			throw new Error('Wrong id.');
		}

		let cliInvoiceReview = new InvoiceReview(this.Config);
		return await cliInvoiceReview.create({
			CustomersKey: id,
			ReviewItems: null,
			ReviewUntil: null,
			T4UsersKey: null
		});
	}

	/**
	 * Retrieving a collection of customer locations.
	 * @summary
	 * Alias for locations?Filter=CustomersKey.Equal({customersKeyString})
	 * @param {Number} id CustomersKey
	 * @param {Object} options Collection Parameters
	 * @param {Number||'All'} options.pageSize Maximum number of records in this collection {number or 'All'}
	 * @param {Number} options.pageIndex Will return those records that follow first X pages
	 * @param {Filter} options.filter Filters are used to limit the set of data returned in the collection
	 * @returns {Receipt}
	 */
	async getLocations(id, options) {
		if (isNaN(id)) {
			throw new Error('Wrong id.');
		}

		if (!options) {
			options = { pageSize: 'All' };
		}

		return await this.execGetRequest(Client.Utils.joinURL([this.Api, id, 'locations', Client.Utils.getListQuery(options, this.defaults)]));
	}


	/**
	 * Retrieving a collection of customer invoices.
	 * @summary
	 * Alias for invoices&Filter=CustomersKey.Equal({customersKeyString}).OR.CorpAccountNumber.Equal({customersKeyString})'
	 * @param {Number} id CustomersKey
	 * @param {Object} options Collection Parameters
	 * @param {Number||'All'} options.pageSize Maximum number of records in this collection {number or 'All'}
	 * @param {Number} options.pageIndex Will return those records that follow first X pages
	 * @param {Filter} options.filter Filters are used to limit the set of data returned in the collection
	 * @returns {Receipt}
	 */
	async getInvoices(id, options) {
		if (isNaN(id)) {
			throw new Error('Wrong id.');
		}

		if (!options) {
			options = { pageSize: 'All' };
		}

		//invoice can be assigned to customer , but corporate should take all of them
		options.filter =
			'(CustomersKey.Equal(' + id + ').OR.CorpAccountNumber.Equal(' + id + '))'
			+ (options.filter ? '.AND.' + options.filter : '');

		// if (options.corpAccountNumber && options.customersKey) {
		// 	filter = ';
		// } else if (options.customersKey) {
		// 	filter = '&Filter=CustomersKey.Equal(' + options.customersKey + ')';
		// }

		let cliInvoices = new Invoices(this.Config);
		return await cliInvoices.list(options);
	}
};
