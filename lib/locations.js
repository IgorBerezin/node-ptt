const Client = require('./client');

/**
 * Main Locations package
 * Represents functionality to request Locations API.
 * @extends {Client}
 * @return {Locations}
 */
const Locations = module.exports = class Locations extends Client {

	/**
	 * Root api endpoint
	 */
	get Api() {
		return 'locations';
	}

	/**
	 * Retrieving a single record
	 * @param {Number} id LocationsKey
	 * @returns {Receipt}
	 */
	async get(id) {
		return super.get(this.Api, id);
	}

	/**
	 * Retrieving a collection of objects
	 * @param {Object} options Collection Parameters
	 * @param {Number||'All'} options.pageSize Maximum number of records in this collection {number or 'All'}
	 * @param {Number} options.pageIndex Will return those records that follow first X pages
	 * @param {Filter} options.filter Filters are used to limit the set of data returned in the collection
	 * @returns {Receipt}
	 */
	async list(options) {
		return await super.list(this.Api, options);
	}

	/**
	 * Creating a location.
	 * @summary
	 * Create: requires an object consisting of the information
	 * for the Location record:
	 * Minimum properties (fields) to set:
	 *    Type - There can be only 1 “BillTo” for a Customer.
	 *        “BillTo” should be used for the Location created when adding a Customer record.
	 *         All other times, Type should be something else “Physical”, “Shipping”, etc.
	 *    Name
	 *    ContactName
	 *    Description
	 *    MainPhone
	 *    AddressLine1
	 *    City
	 *    State
	 *    CountryCode - Must be a valid Country Code (840 is US).
	 *               A list of the defined Countries can be found at the
	 *               <TBSRestServiceURI>/countries endpoint.
	 *    ZipCode - Country Code is used to define a regex for format validation.
	 *           When using 840 (US), Zip Code must be in the format 99999 or 99999-9999
	 *    InCity - This impacts taxing by indicating if the location is within City Limits.
	 * @param {Object} location Location record
	 * @returns {Receipt}
	 */
	async create(location) {
		if (!location) {
			throw new Error('Wrong data.');
		}
		//no need to check the object structure because PTT will do it.
		return await this.execPostRequest(location, this.Api);
	}

	/**
	 * Updating a location.
	 * @summary
	 * Update: requires a Location record
	 * {Location Object}
	 * @param {Object} location Location record
	 * @returns {Receipt}
	 */
	async update(location) {
		if (!location) {
			throw new Error('Wrong data.');
		}

		if (isNaN(location.LocationsKey)) {
			throw new Error('Wrong id.');
		}

		return await this.execPutRequest(location, Client.Utils.joinURL([this.Api, location.LocationsKey]));
	}

	/**
	 * Saving a location.
	 * @summary
	 * Method may be used to Create or Update location record.
	 * @param {Object} location Location record
	 * @returns {Receipt}
	 */
	async save(location) {
		if (!location) {
			throw new Error('Wrong location data.');
		}

		if (location.LocationsKey) {
			return await this.update(location);
		} else {
			return await this.create(location);
		}
	}

	/**
	 * Retrieve Location Defaults
	 * @returns {Receipt}
	 */
	async getDefaults() {
		return await this.execGetRequest(Client.Utils.joinURL([this.Api, 'defaults']));
	}

};