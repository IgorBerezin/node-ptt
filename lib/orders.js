const Client = require('./client');

/**
 * Main Orders package
 * Represents functionality to request Portal Sip Order
 * @extends {Client}
 * @return {Orders}
 */

module.exports = class Orders extends Client {
	get api() {
		return 'orders';
	}
	/**
	 * Creating an Order.
	 * @summary
	 * Create: requires an object consisting of the information
	 * { "AssignedTo": string, "CustomersKey": number, "Description": string, "Expedite": number, "OrderClassification": string, "OrderSummary": string, "OrderType": string }
	 * @param {Object} orderData Data of order
	 * @returns {Receipt}
	 */
	async create(orderData) {
		if (!orderData) {
			throw new Error('No data.');
		}
		// no need to check the object structure because PTT will do it.
		return this.execPostRequest(orderData, this.api);
	}
};
