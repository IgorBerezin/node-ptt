const Client = require('./client');

/**
 * Main Products package
 * Represents functionality to request products
 * @extends {Client}
 * @return {Products}
 */

module.exports = class Products extends Client {
	get api() {
		return 'products';
	}

	/**
	 * Creating a Product.
	 * @summary
	 * Create: requires an object consisting of the information
	 * { "ActiveCarrierNames": null, "ActiveDate": DatetimeAsp, "CompanyKey": number, "CustomersKey": number, "GrandparentProductKey": null | number, "ParentProductID": null | number, "ProductFormsKey": number, "ProductID": string,
	 * "Charges": number, "CompanyKey": number, "LkLineTypesKey": number, "LkTermsKey": number, "ServiceType_ProductType": string, "Status": string }
	 * @param {Object} productData Data of product
	 * @returns {Receipt}
	 */
	async create(productData) {
		if (!productData) {
			throw new Error('No data.');
		}
		// no need to check the object structure because PTT will do it.
		return this.execPostRequest(productData, this.api);
	}
};
