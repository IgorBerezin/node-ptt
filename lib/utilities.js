const Client = require('./client');
const utils = require('./utils');

/**
 * Main Utilities package
 * Represents functionality to request Utilities API.
 * @extends {Client}
 * @return {Utilities}
 */
const Utilities = module.exports = class Utilities extends Client {

	/**
	 * Returns an ASP.NET formatted date time string for something human readable
	 * like 2013-08-07 or 2013-02-14 13:55:27 or special cases like TODAY, NOW, etc.
	 * @param {Date|String} date Date
	 * @returns {Receipt}
	 */
	async getDatetimeAsp(date) {
		let aspDate = Client.Utils.convertDateToLocalISOAspString(date || 'NOW');
		return await this.execPostRequest(aspDate, 'utilities/getdatetime/asp');
	}
	/**
	 * Returns an ASP.NET formatted date time string
	 * like "/Date(1674536400000-0500)/"
	 * @param {Date|Number|String} date Date
	 * @returns {String}
	 */
	getDatetimeAspRaw(date) {
		return utils.convertDateToLocalISOAspDate(date);
	}
};
