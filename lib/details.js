const Client = require('./client');

/**
 * Main Details package
 * Represents functionality to request details
 * @extends {Client}
 * @return {Details}
 */

module.exports = class Details extends Client {
	get api() {
		return 'details';
	}

	/**
	 * Creating a Detail.
	 * @summary
	 * Create: requires an object consisting of the information
	 * { "ChargeClassification": string, "CustomersKey": number, "DetailID": string, "DetailsKey": number, "IntervalCharge": number, "LkDetailsKey": number, "PlanKey": number, "PlanType": number, "Quantity": number,
	 * "ActivationCharge": number, "ChargeClassification": string,  }
	 * @param {Object} detailData Data of detail
	 * @returns {Receipt}
	 */
	async create(detailData) {
		if (!detailData) {
			throw new Error('No data.');
		}
		// no need to check the object structure because PTT will do it.
		return this.execPostRequest(detailData, this.api);
	}
};