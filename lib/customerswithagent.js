const Client = require('./client');
const Locations = require('./locations');

/**
 * Main CustomersWithAgent package
 * Represents functionality to request CustomersWithAgent API.
 * @extends {Client}
 * @return {CustomersWithAgent}
 */
const CustomersWithAgent = module.exports = class CustomersWithAgent extends Client {

	/**
	 * Root api endpoint
	 */
	get Api() {
		return 'customerswithagent';
	}

	/**
	 * Retrieving a single record
	 * @param {Number} id CustomersKey
	 * @returns {Receipt}
	 */
	async get(id) {
		return super.get(this.Api, id);
	}

	/**
	 * Retrieving a collection of objects
	 * @param {Object} options Collection Parameters
	 * @param {Number||'All'} options.pageSize Maximum number of records in this collection {number or 'All'}
	 * @param {Number} options.pageIndex Will return those records that follow first X pages
	 * @param {Filter} options.filter Filters are used to limit the set of data returned in the collection
	 * @returns {Receipt}
	 */
	async list(options) {
		return await super.list(this.Api, options);
	}

	/**
	 * Creating a customer.
	 * @summary
	 * Create: requires an object consisting of the information
	 * for the Customer record as well as the information
	 * for the 'bill to' location:
	 * { "Customer": {Customer Object}, "BillToLocation": {Location Object} }
	 * @param {Object} customer Customer record
	 * @returns {Receipt}
	 */
	async create(customer) {
		if (!customer) {
			throw new Error('Wrong data.');
		}
		//no need to check the object structure because PTT will do it.
		return await this.execPostRequest(customer, this.Api);
	}

	/**
	 * Updating a customer.
	 * @summary
	 * Update: requires a Customer record
	 * {Customer Object}
	 * @param {Object} customer Customer record
	 * @returns {Receipt}
	 */
	async update(customer) {
		if (!customer) {
			throw new Error('Wrong data.');
		}

		if (isNaN(customer.CustomersKey)) {
			throw new Error('Wrong id.');
		}

		return await this.execPutRequest(customer, Client.Utils.joinURL([this.Api, customer.CustomersKey]));
	}

	/**
	 * Saving a customer.
	 * @summary
	 * Method may be used to Create or Update customer record.
	 *
	 * Create: requires an object consisting of the information
	 * for the Customer record as well as the information
	 * for the 'bill to' location:
	 * { "Customer": {Customer Object}, "BillToLocation": {Location Object} }
	 *
	 * Update: requires a Customer record only, but will works with a combined data also
	 * {Customer Object}
	 * { "Customer": {Customer Object}, "BillToLocation": {Location Object} }
	 * @param {Object} customer Customer record
	 * @returns {Receipt}
	 */
	async save(customer) {
		if (!customer) {
			throw new Error('Wrong customer data.');
		}

		if (customer.CustomersKey) {
			return await this.update(customer);
		} else {
			if (customer.Customer && customer.Customer.CustomersKey) {
				let locationResult = {};

				if (customer.BillToLocation) {
					let cliLocations = new Locations(this.Config);
					locationResult = await cliLocations.save(customer.BillToLocation);
				}

				if (locationResult.error) {
					return locationResult;
				}

				let customerResult = await this.update(customer.Customer);

				if (!customerResult.error) {
					//aggregated response of 2 requests
					customerResult.response = {
						Customer: customerResult.response
					};
					if (customer.BillToLocation) {
						customerResult.response.BillToLocation = locationResult.response;
					}
				}


				return customerResult;
			} else {
				return await this.create(customer);
			}
		}
	}
};
