module.exports = {
	Bases: require('./bases'),
	Client: require('./client'),
	Customers: require('./customers'),
	CustomersWithAgent: require('./customerswithagent'),
	InvoiceReview: require('./invoicereview'),
	Invoices: require('./invoices'),
	Locations: require('./locations'),
	Receipt: require('./receipt'),
	Utilities: require('./utilities'),
	Orders: require('./orders'),
	Products: require('./products'),
	Details: require('./details'),
};