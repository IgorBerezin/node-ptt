const Client = require('./client');

/**
 * Main Invoices package
 * Represents functionality to request Invoices API.
 * @extends {Client}
 * @return {Invoices}
 */
const Invoices = module.exports = class Invoices extends Client {

	/**
	 * Root api endpoint
	 */
	get Api() {
		return 'invoices';
	}

	/**
	 * Retrieving a single record
	 * @param {Number} id InvoicesKey
	 * @returns {Receipt}
	 */
	async get(id) {
		return super.get(this.Api, id);
	}

	/**
	 * Retrieving a collection of objects
	 * @param {Object} options Collection Parameters
	 * @param {Number||'All'} options.pageSize Maximum number of records in this collection {number or 'All'}
	 * @param {Number} options.pageIndex Will return those records that follow first X pages
	 * @param {Filter} options.filter Filters are used to limit the set of data returned in the collection
	 * @returns {Receipt}
	 */
	async list(options) {
		return await super.list(this.Api, options);
	}

	/**
	 * Retrieve Custer's invoice for a specific billing cycle
	 * @returns {Receipt}
	 */
	async getInvoiceLink(billingCycleID, customersKey, invoiceType) {
		if (isNaN(customersKey)) {
			throw new Error('Wrong customersKey.');
		}
		if (!(billingCycleID)) {
			throw new Error('Wrong billingCycleID.');
		}
		if (!(invoiceType)) {
			throw new Error('Wrong invoiceType.');
		}
		return await this.execGetRequest(Client.Utils.joinURL([this.Api, billingCycleID, customersKey, invoiceType, 'link']));
	}

};