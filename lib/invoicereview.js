const Client = require('./client');

/**
 * Main InvoiceReview package
 * Represents functionality to request InvoiceReview API.
 * @extends {Client}
 * @return {InvoiceReview}
 */
const InvoiceReview = module.exports = class InvoiceReview extends Client {

	/**
	 * Root api endpoint
	 */
	get Api() {
		return 'invoicereview';
	}

	/**
	 * Retrieving a single record
	 * @param {Number} id InvoiceReviewKey
	 * @returns {Receipt}
	 */
	async get(id) {
		return super.get(this.Api, id);
	}

	/**
	 * Retrieving a collection of objects
	 * @param {Object} options Collection Parameters
	 * @param {Number||'All'} options.pageSize Maximum number of records in this collection {number or 'All'}
	 * @param {Number} options.pageIndex Will return those records that follow first X pages
	 * @param {Filter} options.filter Filters are used to limit the set of data returned in the collection
	 * @returns {Receipt}
	 */
	async list(options) {
		return await super.list(this.Api, options);
	}

	/**
	 * Creating an InvoiceReview.
	 * @param {Object} invoiceReview InvoiceReview record
	 * @returns {Receipt}
	 */
	async create(invoiceReview) {
		if (!invoiceReview) {
			throw new Error('Wrong data.');
		}

		return await this.execPostRequest(invoiceReview, this.Api);
	}

};