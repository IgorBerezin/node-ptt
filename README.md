# node-ptt

NodeJs Client library for Precision TBSRestService

## Supported Versions
This SDK stable for node versions 8 and above

## Release Notes
| Version | Notes |
|:--|:--|
| 1.0.0 | Initial |
| 1.2.0 | Add POST request for order, product, detail |

## Install

Run

```
npm install node-ptt
```

## Configuration
Precision can provide clients with a “test system” for development and testing purposes.  Please contact Precision with any questions about how to use the API or if you would like to have a test platform setup for your company.

Options:

  * `authorization` **Required**. Authorization block
    * `type` Auth type. [Basic]
    * `username` **Required**. PTT login
    * `password` **Required**. PTT password
  * `url` **Required**. Uniform Resource Locator's block
    * `host` **Required**. PTT host
    * `port` Port Number.
    * `path` **Required**. The path identifies the specific api resource
  * `title` Client title

## Usage

### General principles
When you receive an API response, it will always contain the following information.

* `duration` Request duration
* `endpoint` API Endpoint
* `method` HTTP Method. ['GET', 'POST', 'PUT', 'DELETE']
* `headers` Headers
* `request` Body
* `error` Response Error (if any)
* `response` Either the Response object, or Error Message

Example:
```Javascript
const ptt = require('node-ptt');
const config = {
	'authorization': {
		'type': 'Basic',
		'username': 'FakeUserName',
		'password': 'FakePassword'
	},
	'url': {
		'host': 'https://precisebillonline.com/',
		'path': 'TBSDemo/RestService/TBSRestService'
	}
};

const utilities = new ptt.Utilities(config);

getDatetimeAsp = async function (date, context) {
	let result = await utilities.getDatetimeAsp(date);
	console.log('Result: ', result);
	if (result.error) {
		throw result.error;
	}
	return result.response;
};

getDatetimeAsp('sdsd').then();

```
Console:
```Javascript
Result:  { duration: 8041,
  endpoint: 'https://precisebillonline.com/TBSDemo/RestService/TBSRestService/utilities/getdatetime/asp',
  method: 'POST',
  headers: 
   { Accept: '*/*',
     'Content-Type': 'application/json',
     'Content-Length': 6,
     Authorization: 'Basic RmFrZVVzZXJOYW1lOkZha2VQYXNzd29yZA==' },
  request: '"sdsd"',
  error: Error: Could not parse as date.,
  response: 'Could not parse as date.' }
```

### API Objects

#### Customers
Get Customer:

```javascript
const customers = new ptt.Customers(config);
return await customers.get(id);

```

#### Utilities
Get an ASP.NET formatted date:

```javascript
const utilities = new ptt.Utilities(config);
return await utilities.getDatetimeAsp(date);

```

## Examples
There is an 'examples' folder in the source tree that shows how each of the API objects work with simple example code.  To run the examples:

```bash
$ cd examples
$ cp config.js.example config.js
```
Edit the config.js to match your PTT credentials and run the examples individually.  e.g.

```bash
node utilities-get-datetime-asp-example.js
```
If the examples take command line parameters, you will get the usage by just executing the individual script.

## Troubleshooting
- We're using Mocha as a test framework
- We can write our JavaScript in ES6, and compile it back to ES5 using Babel
- We can debug our tests using Node's built-in debugger, or using the Chrome DevTools
- We're measuring our test coverage using Istanbul

# Notes

Created as a module for another project. Some features still unavailable.

## Tests
To run single test file from test folder\
--opts for older mocha versions (--config for mocha > 6.0.0 version)

```bash
npx mocha --opts ./mocha.opts bases.js
```
# License
MIT (see LICENSE)

# Author
Igor Berezin <ig.v.berezin@gmail.com>